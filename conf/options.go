// SPDX-FileCopyrightText: 2020-2021 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package conf

import "gitlab.alpinelinux.org/Cogitri/aports-qa-bot/Services"

// Options holds all configuration options for the server and poller pat of aports-qa-bot
type Options struct {
	GitlabToken string
	ProxyURL    string
	PollerCron  string
	PollerIDs   []int
	ServerPort  uint
	Services    Services.KnownServices
	LogLevel    string
}
