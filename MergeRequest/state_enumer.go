// Code generated by "enumer -type=State -transform=lower"; DO NOT EDIT.

package MergeRequest

import (
	"fmt"
	"strings"
)

const _StateName = "closedlockedmergedopened"

var _StateIndex = [...]uint8{0, 6, 12, 18, 24}

const _StateLowerName = "closedlockedmergedopened"

func (i State) String() string {
	if i < 0 || i >= State(len(_StateIndex)-1) {
		return fmt.Sprintf("State(%d)", i)
	}
	return _StateName[_StateIndex[i]:_StateIndex[i+1]]
}

// An "invalid array index" compiler error signifies that the constant values have changed.
// Re-run the stringer command to generate them again.
func _StateNoOp() {
	var x [1]struct{}
	_ = x[Closed-(0)]
	_ = x[Locked-(1)]
	_ = x[Merged-(2)]
	_ = x[Opened-(3)]
}

var _StateValues = []State{Closed, Locked, Merged, Opened}

var _StateNameToValueMap = map[string]State{
	_StateName[0:6]:        Closed,
	_StateLowerName[0:6]:   Closed,
	_StateName[6:12]:       Locked,
	_StateLowerName[6:12]:  Locked,
	_StateName[12:18]:      Merged,
	_StateLowerName[12:18]: Merged,
	_StateName[18:24]:      Opened,
	_StateLowerName[18:24]: Opened,
}

var _StateNames = []string{
	_StateName[0:6],
	_StateName[6:12],
	_StateName[12:18],
	_StateName[18:24],
}

// StateString retrieves an enum value from the enum constants string name.
// Throws an error if the param is not part of the enum.
func StateString(s string) (State, error) {
	if val, ok := _StateNameToValueMap[s]; ok {
		return val, nil
	}

	if val, ok := _StateNameToValueMap[strings.ToLower(s)]; ok {
		return val, nil
	}
	return 0, fmt.Errorf("%s does not belong to State values", s)
}

// StateValues returns all values of the enum
func StateValues() []State {
	return _StateValues
}

// StateStrings returns a slice of all String values of the enum
func StateStrings() []string {
	strs := make([]string, len(_StateNames))
	copy(strs, _StateNames)
	return strs
}

// IsAState returns "true" if the value is listed in the enum definition. "false" otherwise
func (i State) IsAState() bool {
	for _, v := range _StateValues {
		if i == v {
			return true
		}
	}
	return false
}
