// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoStale

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Cogitri/aports-qa-bot/MergeRequest"
)

type Service struct {
	dryRun       bool
	gitlabClient *gitlab.Client
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

// RepoConfig holds per-repository configuration that's located in a config file in the repo.
type RepoConfig struct {
	NotStaleLabels []string
	StaleDays      uint
	StaleLabel     string
	StaleMessage   string
}

func (s *Service) getConfigForPid(pid int, log *zerolog.Logger) (RepoConfig, error) {
	log.Info().
		Msg("getting project configuration.Configuration")

	config := RepoConfig{
		NotStaleLabels: []string{"status:mr-hold"},
		StaleDays:      30,
		StaleLabel:     "status:mr-stale",
		StaleMessage:   "Sorry to bother you @%s,\n\nbut we've detected that this merge request hasn't seen any recent activity. If you need help or want to discuss your approach with developers you can ping `@%s`. You can also ask on IRC on `#alpine-devel` on Freenode.net. If no further activity occurs in this MR, Alpine developers may close it in the future.\n\nThanks for your contribution.",
	}

	cfgFile, _, err := s.gitlabClient.RepositoryFiles.GetFile(
		pid,
		".gitlab/aports-qa-bot.json",
		&gitlab.GetFileOptions{Ref: gitlab.String("master")},
	)
	if err != nil {
		return config, err
	}

	data, base64Err := base64.StdEncoding.DecodeString(cfgFile.Content)

	if base64Err != nil {
		return config, base64Err
	}

	err = json.Unmarshal(data, &config)

	if err != nil {
		return config, err
	}

	log.Info().
		Str("NotStaleLabels", strings.Join(config.NotStaleLabels, ";")).
		Uint("StaleDays", config.StaleDays).
		Str("StaleLabel", config.StaleLabel).
		Str("StaleMessage", config.StaleMessage).
		Msg("got project Configuration")

	return config, nil
}

func (s *Service) pollStale(pid int, repoConfig RepoConfig) ([]*gitlab.MergeRequest, error) {
	labels := gitlab.Labels{repoConfig.StaleLabel}
	labels = append(labels, repoConfig.NotStaleLabels...)
	mrs, _, err := s.gitlabClient.MergeRequests.ListProjectMergeRequests(
		pid,
		&gitlab.ListProjectMergeRequestsOptions{
			State:         gitlab.String(MergeRequest.Opened.String()),
			UpdatedBefore: gitlab.Time(time.Now().Add(time.Hour * 24 * time.Duration(repoConfig.StaleDays) * -1)),
			NotLabels:     &labels,
		},
	)
	if err != nil {
		return nil, err
	}
	return mrs, nil
}

// Process processes the MergeRequestStaleJob and may call Gitlab's API to mark a MR as stale
func (s Service) Process(pid int, log *zerolog.Logger) {
	sLog := log.With().
		Str("service", "AutoStale").
		Logger()

	RepoConfig, err := s.getConfigForPid(pid, &sLog)
	if err != nil {
		sLog.Warn().
			Err(err).
			Msg("failed to get repo configuration using default")
	}

	mrs, err := s.pollStale(pid, RepoConfig)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to poll for stale Merge Requests")
		return
	}

	for i := range mrs {
		mr := mrs[i]

		// Get the right Assignee
		var assignee string
		if mr.Assignee != nil {
			assignee = mr.Assignee.Username
		} else {
			assignee = "team/mentors"
		}

		log := sLog.With().Int("MR", mr.IID).Logger()

		log.Info().
			Str("ping", mr.Author.Username).
			Str("contact", assignee).
			Msg("processing merge request")

		if !s.dryRun {
			_, _, err := s.gitlabClient.MergeRequests.UpdateMergeRequest(
				mr.TargetProjectID,
				mr.IID, &gitlab.UpdateMergeRequestOptions{
					AddLabels: &gitlab.Labels{RepoConfig.StaleLabel},
				},
			)
			if err != nil {
				log.Error().
					Err(err).
					Msg("failed to add stale label to Merge Request")
				return
			}

			noteText := fmt.Sprintf(RepoConfig.StaleMessage, mr.Author.Username, assignee)
			_, _, err = s.gitlabClient.Notes.CreateMergeRequestNote(
				mr.TargetProjectID,
				mr.IID,
				&gitlab.CreateMergeRequestNoteOptions{Body: gitlab.String(noteText)},
			)
			if err != nil {
				log.Error().
					Err(err).
					Msg("failed to add stale note to Merge Request")
				return
			}
		}
	}
}
