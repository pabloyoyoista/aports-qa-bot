// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package MinimumRequiredSettings

import (
	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Cogitri/aports-qa-bot/MergeRequest"
)

type Service struct {
	dryRun       bool
	gitlabClient *gitlab.Client
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Reopen: true,
	MergeRequest.Open:   true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "MinimumRequiredSettings").
		Logger()

	sLog.Info().Msg("starting")

	if !s.dryRun {
		_, _, err := s.gitlabClient.MergeRequests.UpdateMergeRequest(
			payload.ObjectAttributes.TargetProjectID,
			payload.ObjectAttributes.IID,
			&gitlab.UpdateMergeRequestOptions{
				AllowCollaboration: gitlab.Bool(true),
				RemoveSourceBranch: gitlab.Bool(true),
			})
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed")
			return
		}
	}

	sLog.Info().Msg("finished")
}
