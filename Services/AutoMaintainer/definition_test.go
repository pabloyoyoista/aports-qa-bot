// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoMaintainer

import (
	"io"
	"net/http"
	"testing"

	"gitlab.alpinelinux.org/Cogitri/aports-qa-bot/mocklab"
)

// Taken from server_test.go, remove them once split
func assertEqStr(t *testing.T, expected, got string) {
	t.Helper()
	if expected != got {
		t.Errorf("Expected '%s', got '%s'", expected, got)
	}
}

func Test_hasUserSimple(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/users", func(w http.ResponseWriter, r *http.Request) {
		mocklab.MustWriteHTTPResponse(t, w, "testdata/has_user_simple.json")
	})

	user, err := hasUser(client, "foo@bar.org")
	if err != nil {
		t.Errorf("expected error = nil, got = %s", err)
	}
	assertEqStr(t, "foo", user.Username)
}

func Test_hasUserNoMatch(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/users", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "[]"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	_, err := hasUser(client, "user@doesnotexist.gov")
	if err == nil {
		t.Errorf(`expected error = no users found with email address "user@doesnotexist.gov", got = nil`)
	}
	if err.Error() != `no users found with email address "user@doesnotexist.gov"` {
		t.Errorf(`unexpected error: %v`, err)
	}
}

func Test_hasUserMultiMatch(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/users", func(w http.ResponseWriter, r *http.Request) {
		mocklab.MustWriteHTTPResponse(t, w, "testdata/has_user_multi_match.json")
	})

	_, err := hasUser(client, "foo@bar.org")
	if err == nil {
		t.Errorf(`expected error = too many users found with email address "foo@bar.org"`)
	}
	if err.Error() != `too many users found with email address "foo@bar.org"` {
		t.Errorf(`unexpected error: %v`, err)
	}
}

func Test_extractMaintainerEmailSimple(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: Foo <foo@bar.org>"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	address, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err != nil {
		t.Errorf("got unexpected error: %v", err)
	}
	assertEqStr(t, "foo@bar.org", address)
}

func Test_extractMaintainerEmailSimpleNoName(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: <foo@bar.org>"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	address, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err != nil {
		t.Errorf("got unexpected error: %v", err)
	}
	assertEqStr(t, "foo@bar.org", address)
}

func Test_extractMaintainerEmailNoMaintainer(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, ""); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	_, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err == nil {
		t.Error(`expected error: no maintainer field`)
	}
	if err.Error() != `no maintainer field` {
		t.Errorf("unexpected error: %v", err)
	}
}

func Test_extractMaintainerEmailBadFormatUnclosedAngleAddr(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: Foo <foo@bar.org"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	_, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err == nil {
		t.Error(`expected error: mail: unclosed angle-addr`)
	}
	if err.Error() != `mail: unclosed angle-addr` {
		t.Errorf("unexpected error: %v", err)
	}
}

func Test_extractMaintainerEmailMissingAddress(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: Foo"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	_, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err == nil {
		t.Error(`expected error: mail: missing '@' or angle-addr`)
	}
	if err.Error() != `mail: missing '@' or angle-addr` {
		t.Errorf("unexpected error: %v", err)
	}
}

func Test_extractMaintainerEmailOnlyName(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: Foo"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	_, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err == nil {
		t.Error(`expected error: mail: missing '@' or angle-addr`)
	}
	if err.Error() != `mail: missing '@' or angle-addr` {
		t.Errorf("unexpected error: %v", err)
	}
}

func Test_extractMaintainerEmailEmpty(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer:"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	_, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err == nil {
		t.Error(`expected error: mail: no address`)
	}
	if err.Error() != `mail: no address` {
		t.Errorf("unexpected error: %v", err)
	}
}

func Test_extractMaintainerEmailTag(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: Foo <foo+alpine@bar.org>"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	address, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err != nil {
		t.Errorf("got unexpected error: %v", err)
	}
	assertEqStr(t, "foo@bar.org", address)
}

func Test_extractMaintainerEmailMultipleTags(t *testing.T) {
	mux, server, client := mocklab.Setup(t)
	defer mocklab.Teardown(server)

	mux.HandleFunc("/api/v4/projects/1/repository/files/APKBUILD/raw", func(w http.ResponseWriter, r *http.Request) {
		if _, err := io.WriteString(w, "# Maintainer: Foo <foo+alpine+aports@bar.org>"); err != nil {
			t.Fatalf("error writing response: %v", err)
		}
	})

	address, err := extractMaintainerEmail(client, 1, "APKBUILD", "master")
	if err != nil {
		t.Errorf("got unexpected error: %v", err)
	}
	assertEqStr(t, "foo@bar.org", address)
}
