// SPDX-FileCopyrightText: 2020-2021 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package Services

// State describes the wanted state of a Service from aports-qa-bot
type State int

//go:generate go run github.com/dmarkham/enumer -type=State -json -text
const (
	Enabled  State = iota // Enabled means the service will run
	Disabled              // Disabled means the service will not run
	DryRun                // Dry-Run means the service will run but won't perform actions that change the states
	// e.g. a service that does auto-labeling will calculate all the label changes to be
	// done but won't implement them
)
